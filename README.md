# Matching Game

## Entwicklung
~~~
yarn
mtlg serve module
~~~

# Spielmodi und direkter Spielaufruf
## Spielmodi

Modus|Domainzusatz
--- | ---    
Computer-Science | ...de/dev/zuordnunginf<b>?CS</b>
alle Spiele | ohne
alle Spiele außer Computer-Science | ...de/dev/zuordnunginf<b>?CASUAL</b>

## direkter Spielaufruf

Spiel|Domainzusatz
--- | ---    
Informatik - Reguläre Ausdrücke | ...de/dev/zuordnunginf<b>?regex</b>
Informatik - Eingabe-Verarbeitung-Ausgabe | ...de/dev/zuordnunginf<b>?eva</b>
Chemie - Moleküle | ...de/dev/zuordnunginf<b>?chemie</b>
Deutsch - Grammatik | ...de/dev/zuordnunginf<b>?word</b>
Biologie - Tiere und Lebensräume | ...de/dev/zuordnunginf<b>?animal</b>
