# Bildquellen

## Wattenmeer

Bild|Lizenz 
--- | ---------------
<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/26/Peringia_ulvae_01.JPG/1024px-Peringia_ulvae_01.JPG" width="100"> | Gemeine Wattschnecke, H. Zell, CC BY-SA 3.0, https://creativecommons.org/licenses/by-sa/3.0/, https://upload.wikimedia.org/wikipedia/commons/thumb/2/26/Peringia_ulvae_01.JPG/1024px-Peringia_ulvae_01.JPG
<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/17/Carcinus_maenas.jpg/1280px-Carcinus_maenas.jpg" width="100"> | Gemeine Strandkrabbe, Hans Hillewaert, CC BY-SA 4.0, https://creativecommons.org/licenses/by-sa/4.0/, https://upload.wikimedia.org/wikipedia/commons/thumb/1/17/Carcinus_maenas.jpg/1280px-Carcinus_maenas.jpg
<img src="https://upload.wikimedia.org/wikipedia/commons/8/83/Cerastoderma_lamarcki.jpg" width="100"> | Herzmsuchel, Jan Johan ter Poorten, CC BY-SA 3.0, https://creativecommons.org/licenses/by-sa/3.0/, https://upload.wikimedia.org/wikipedia/commons/8/83/Cerastoderma_lamarcki.jpg
<img src="https://fischratgeber.wwf.at/wp-content/uploads/2016/10/287829.jpg" width="100"> | Nordseegarnele, WWF Österreich, CC BY-SA 3.0, https://creativecommons.org/licenses/by-sa/3.0/, https://fischratgeber.wwf.at/wp-content/uploads/2016/10/287829.jpg
<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/4b/Haematopus_ostralegus_He2.jpg/1024px-Haematopus_ostralegus_He2.jpg" width="100"> | Austernfischer, Andreas Trepte, CC BY-SA 3.0, https://creativecommons.org/licenses/by-sa/3.0/, https://upload.wikimedia.org/wikipedia/commons/thumb/4/4b/Haematopus_ostralegus_He2.jpg/1024px-Haematopus_ostralegus_He2.jpg
<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Herring_Gull_in_flight.jpg/1024px-Herring_Gull_in_flight.jpg" width="100"> | Silbermöwe, Andreas Trepte, CC BY-SA 3.0, https://creativecommons.org/licenses/by-sa/3.0/, https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Herring_Gull_in_flight.jpg/1024px-Herring_Gull_in_flight.jpg
<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/2d/Tadorna_tadorna_%28Linnaeus%2C_1758%29.jpg/1280px-Tadorna_tadorna_%28Linnaeus%2C_1758%29.jpg" width="100"> | Brandgans, Michael Gäbler, CC BY-SA 3.0, https://creativecommons.org/licenses/by-sa/3.0/, https://upload.wikimedia.org/wikipedia/commons/thumb/2/2d/Tadorna_tadorna_%28Linnaeus%2C_1758%29.jpg/1280px-Tadorna_tadorna_%28Linnaeus%2C_1758%29.jpg

## Bergwelt

Bild|Lizenz 
--- | ---------------
<img src="https://cdn.pixabay.com/photo/2017/11/02/11/02/adler-2910746_960_720.png" width="100"> | Murmeltier, Alexas_Fotos, Pixabay License, https://pixabay.com/de/service/license/, https://cdn.pixabay.com/photo/2017/11/02/11/02/adler-2910746_960_720.png
<img src="https://cdn.pixabay.com/photo/2018/01/21/09/42/schneehase-3096164_960_720.jpg" width="100"> | Schneehase, Capri23auto, Pixabay License, https://pixabay.com/de/service/license/, https://cdn.pixabay.com/photo/2018/01/21/09/42/schneehase-3096164_960_720.jpg
<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/23/Bartgeier_Gypaetus_barbatus_front_Richard_Bartz.jpg/800px-Bartgeier_Gypaetus_barbatus_front_Richard_Bartz.jpg" width="100"> | Bartgeier, Richard Bartz, CC BY-SA 3.0, https://creativecommons.org/licenses/by-sa/3.0/, https://upload.wikimedia.org/wikipedia/commons/thumb/2/23/Bartgeier_Gypaetus_barbatus_front_Richard_Bartz.jpg/800px-Bartgeier_Gypaetus_barbatus_front_Richard_Bartz.jpg
<img src="https://live.staticflickr.com/7438/16169333530_5a00d0b1cf_h_d.jpg" width="100"> | Hermelin, hardi_630, CC 0, https://creativecommons.org/publicdomain/mark/1.0/, https://live.staticflickr.com/7438/16169333530_5a00d0b1cf_h_d.jpg
<img src="https://cdn.pixabay.com/photo/2020/03/28/16/21/bear-4977657_960_720.jpg" width="100"> | Braunbär, humlan, Pixabay License, https://pixabay.com/de/service/license/, https://cdn.pixabay.com/photo/2020/03/28/16/21/bear-4977657_960_720.jpg
<img src="https://cdn.pixabay.com/photo/2019/05/19/17/17/gentian-4214580_960_720.jpg" width="100"> | Enzian, Trimlack, Pixabay License, https://pixabay.com/de/service/license/, https://cdn.pixabay.com/photo/2019/05/19/17/17/gentian-4214580_960_720.jpg
<img src="https://cdn.pixabay.com/photo/2013/09/12/16/15/alpine-edelwei-181708_960_720.jpg" width="100"> | Edelweiss, Hans, Pixabay License, https://pixabay.com/de/service/license/, https://cdn.pixabay.com/photo/2013/09/12/16/15/alpine-edelwei-181708_960_720.jpg

## Wald

Bild|Lizenz 
--- | ---------------
<img src="https://cdn.pixabay.com/photo/2016/11/02/17/03/christmas-tree-1792267_960_720.jpg" width="100"> | christmas tree, maciej326, Pixabay License, https://pixabay.com/de/service/license/, https://cdn.pixabay.com/photo/2016/11/02/17/03/christmas-tree-1792267_960_720.jpg
<img src="https://cdn.pixabay.com/photo/2018/09/15/20/21/cone-3680270_960_720.png" width="100"> | kiefer zapfen, andrejbujna, Pixabay License, https://pixabay.com/de/service/license/, https://cdn.pixabay.com/photo/2018/09/15/20/21/cone-3680270_960_720.png
<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/6f/Fly_Agaric_mushroom_04_edit.jpg/1280px-Fly_Agaric_mushroom_04_edit.jpg" width="100"> | Fliegenpilz in Neuseeland, Tony Wills, CC BY-SA 3.0, https://creativecommons.org/licenses/by-sa/3.0/, https://upload.wikimedia.org/wikipedia/commons/thumb/6/6f/Fly_Agaric_mushroom_04_edit.jpg/1280px-Fly_Agaric_mushroom_04_edit.jpg
<img src="https://upload.wikimedia.org/wikipedia/commons/2/23/Morchella_esculenta_08.jpg" width="100"> | Fruchtkörper der Speise-Morchel, J. Marqua, CC BY-SA 3.0, https://creativecommons.org/licenses/by-sa/3.0/, https://upload.wikimedia.org/wikipedia/commons/2/23/Morchella_esculenta_08.jpg
<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/f/ff/Wildsau_im_wilderlebnispark_daun.jpg/1280px-Wildsau_im_wilderlebnispark_daun.jpg" width="100"> | ausgewachsenes Wildschwein, Superbass, CC BY-SA 3.0, https://creativecommons.org/licenses/by-sa/3.0/, https://upload.wikimedia.org/wikipedia/commons/thumb/f/ff/Wildsau_im_wilderlebnispark_daun.jpg/1280px-Wildsau_im_wilderlebnispark_daun.jpg
<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/42/Kupine_dozrijevaju_na_stabljici_%28Croatia%29.1.jpg/1280px-Kupine_dozrijevaju_na_stabljici_%28Croatia%29.1.jpg" width="100"> | Früchte in verschiedenen Stadien der Reifung, Silverije, CC BY-SA 3.0, https://creativecommons.org/licenses/by-sa/3.0/, https://upload.wikimedia.org/wikipedia/commons/thumb/4/42/Kupine_dozrijevaju_na_stabljici_%28Croatia%29.1.jpg/1280px-Kupine_dozrijevaju_na_stabljici_%28Croatia%29.1.jpg
<img src="https://upload.wikimedia.org/wikipedia/commons/3/3b/Formica_rufa_Quadrat.jpg" width="100"> | rote Waldameise, Richard Bartz, CC BY-SA 3.0, https://creativecommons.org/licenses/by-sa/3.0/, https://upload.wikimedia.org/wikipedia/commons/3/3b/Formica_rufa_Quadrat.jpg

https://pixabay.com/de/photos/eichh%C3%B6rnchen-tier-niedlich-493790/

https://pixabay.com/de/photos/tier-attraktiv-sch%C3%B6ne-junge-brown-1238228/

https://de.wikipedia.org/wiki/Uhu#/media/Datei:Tierpark_Berlin,_Bubo_bubo_omissus,_252-357.JPG

https://pixabay.com/de/photos/buntspecht-specht-waldvogel-2482669/

https://pixabay.com/de/photos/reh-freigestellt-rehkitz-jung-wald-3499921/

https://pixabay.com/de/photos/heidelbeere-obst-blau-539135/

https://pixabay.com/de/photos/b%C3%A4rlauch-bearish-zwiebeln-bl%C3%BCte-3064095/

https://pixabay.com/de/photos/natur-pflanzen-blatt-im-freien-3151534/

https://de.wikipedia.org/wiki/Echte_Schl%C3%BCsselblume#/media/Datei:Primula_veris_0x.JPG

https://commons.wikimedia.org/wiki/File:Teichmolch-triturus-vulgaris.jpg

https://de.wikipedia.org/wiki/Graureiher#/media/Datei:Graureiher.Krughorn.jpg

https://pixabay.com/de/illustrations/ente-stockente-erpel-transparent-1978680/

https://pixabay.com/de/illustrations/tier-gans-federvieh-graugans-2057642/

https://pixabay.com/de/illustrations/gras-schilf-freigestellt-gr%C3%BCn-2010932/

https://pixabay.com/de/photos/seerose-teichrose-wasserpflanze-1592771/

https://pixabay.com/de/photos/fisch-transparent-freigestellt-3435849/

https://pixabay.com/de/vectors/tier-fisch-ozean-meer-1300170/

https://pixabay.com/de/photos/wasserl%C3%A4ufer-natur-evertebrat-3322665/

https://upload.wikimedia.org/wikipedia/commons/9/9d/ElodeaCanadensis.jpg

https://pixabay.com/de/illustrations/baum-laubbaum-weide-digital-art-1511609/

https://de.wikipedia.org/wiki/Pfeilkraut#/media/Datei:SagittariaSagittifoliaInflorescence2.jpg

https://de.wikipedia.org/wiki/Teichrohrs%C3%A4nger#/media/Datei:PleidelsheimerWiesentalTeichrohrsaenger.jpg

https://pixabay.com/de/photos/libelle-fl%C3%BCgel-insekt-natur-3456317/

https://pixabay.com/de/photos/fischotter-nasse-otter-zoo-pelz-3801698/

https://commons.wikimedia.org/wiki/File:Nutria_in_der_Bonner_Rheinaue.jpg

https://pixabay.com/de/photos/wilde-m%C3%B6hre-bl%C3%BCtendolde-bl%C3%BCte-rosa-592586/