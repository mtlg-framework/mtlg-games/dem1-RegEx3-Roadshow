
let sessionId // eslint-disable-line no-unused-vars

// expose framework for debugging
window.MTLG = MTLG
window.window.createjs = createjs
// ignite the framework
document.addEventListener('DOMContentLoaded', MTLG.init)

// add moduls
require('mtlg-modul-menu')
require('mtlg-moduls-utilities')
require('mtlg-modul-lightparse')
// require('mtlg-module-dd')
// require('mtlg-module-remoteteacher')
require('mtlg-module-datacollector-xapi')

// load configuration
require('../manifest/device.config.js')
require('../manifest/game.config.js')
require('../manifest/game.settings.js')

// load translations
require('../lang/lang.js')

// load game components
const MENU = require('./menu/mainMenu.js')
const SETTINGS = require('./menu/settings.js')
const INITGAMES = require('./game/utils/initGames.js')
const GAMEMENU = require('./game/utils/gameMenu.js')
const HELPMENU = require('./game/utils/helpMenu.js')
const RESULTSCREEN = require('./game/utils/resultScreen.js')

/**
 * Init function for the game.
 * @params options: The options parsed from the manifest files/ Defaults.
 */
const initGame = function (pOptions) {
  // Initialize game
  // Set default var values
  setupVars(pOptions)

  // Define l as abbreveation  for translations.
  // l = MTLG.lang.getString

  // Initialize levels and Menus:
  // The MTLG framework uses the lifecycle manager (lc) to handle level progression,
  // resetting levels and to define a starting point for the game.
  // Use the functions registerMenu and registerLevel to define your levels.
  // The starting point of the game will be the registered menu, if one is present,
  // or the game with the highest value when passed the empty game state.
  // Register Menu

  const searchedGame = (window.location.search).toUpperCase()
  console.log('Es wurde folgende Eingabe in der Suchleiste getätigt: ' + searchedGame + '// The following entry was made in the search bar: ' + searchedGame)

  // selection of a specific game via the search bar
  // the conditions ?CS and ?CASUAL are important for the selection of the other game modes
  if (window.location.search && searchedGame !== '?CS' && searchedGame !== '?CASUAL') {
    sessionInit()
    switch (searchedGame) {
      case '?REGEX':
        MTLG.getSettings().default.game = 'RegEx'
        break
      case '?CHEMIE':
        MTLG.getSettings().default.game = 'Chemie Game'
        break
      case '?WORD':
        MTLG.getSettings().default.game = 'Word Game'
        break
      case '?EVA':
        MTLG.getSettings().default.game = 'EVA Game'
        break
      case '?ANIMAL':
        MTLG.getSettings().default.game = 'Animal und Plant Game'
        break
      default:
        break
    }
  } else if (!window.location.search || searchedGame === '?CS' || searchedGame === '?CASUAL') {
    MTLG.lc.registerMenu(MENU.drawMainMenu)
    console.log('Das Kreismenü wurde erfolgreich geladen. // The circle menu was loaded successfully.')
  }

  // Register levels
  // Levels consist of two parts: the first parameter is the entrance function,
  // that sets the level up and renders it. The second parameter as a check function,
  // that receives the current gamestate as paramter and returns a value between 0 and 1.
  // The lifecycle manager starts the level with the highest return value in the next step.

  MTLG.lc.registerLevel(INITGAMES.gameSelect, INITGAMES.gameSelectCheck)

  // register settings

  MTLG.lc.registerLevel(SETTINGS.drawSettings, SETTINGS.checkSettings) // settings

  // register games

  MTLG.lc.registerLevel(GAMEMENU.drawGameMenu, INITGAMES.checkLevel1)

  MTLG.lc.registerLevel(HELPMENU.showHelpMenuBeforeGameStarts, INITGAMES.checkLevel2)

  MTLG.lc.registerLevel(INITGAMES.templateGame, INITGAMES.checkLevel3)

  // save the value of all matches
  MTLG.loadSettings({
    default: {
      countAllMissMatches: 0,
      countAllRightMatches: 0
    }
  })

  MTLG.lc.registerLevel(RESULTSCREEN.showResultScreen, INITGAMES.checkLevel4)

  // setting whether the GameMenu, the Help menu or the result screen should be viewed
  MTLG.loadSettings({
    default: {
      skipMenu: true,
      skipHelpMenu: false,
      skipResultScreen: false
    }
  })

  MTLG.menu.addSubMenu(function () {
    MTLG.loadSettings({
      default: {
        skipHelpMenu: MTLG.getSettings().default.skipHelpMenu !== true
      }
    })
    MTLG.loadSettings({
      default: {
        skipMenu: MTLG.getSettings().default.skipMenu !== true
      }
    })
    MTLG.loadSettings({
      default: {
        skipMenu: MTLG.getSettings().default.skipResultScreen !== true
      }
    })
  })

  // Init is done
  console.log('Game Init function called')

  // Create and join room for remoteteacher module
  // TODO: via timeout is bad, a hook should be provided by ddd

  // disable RT
  // if (!window.location.search.includes('pupil') && !window.location.search.includes('teacher')) { return }

  // setTimeout(() => {
  //   const ensureRoomExists = new Promise((resolve, reject) => {
  //     MTLG.distributedDisplays.rooms.getAllRooms(rooms => {
  //       if (rooms.RT_tabula) return resolve()
  //       MTLG.distributedDisplays.rooms.createRoom('RT_tabula', result => {
  //         if (result && result.success) resolve()
  //         else reject(result.reason)
  //       })
  //     })
  //   })
  //   ensureRoomExists.catch(err => console.warn('failed to create classroom: ' + err)).then(() => {
  //     // join room
  //     MTLG.distributedDisplays.rooms.joinRoom('RT_tabula', result => {
  //       if (result && result.success) {
  //         console.log('successfully joined classroom')
  //         // start sharing as teacher or pupil
  //         if (window.location.search.includes('teacher')) {
  //           MTLG.remoteteacher.teacherView.init('RT_tabula')
  //           MTLG.lc.registerMenu(MTLG.remoteteacher.teacherView.draw)
  //           MTLG.lc.goToMenu()
  //           // MTLG.remoteteacher.teacherView.draw()
  //         } else {
  //           MTLG.remoteteacher.playerView.init('RT_tabula')
  //           MTLG.remoteteacher.playerView.forSharing(MTLG.getStage())
  //         }
  //       } else console.warn('failed to join classroom: ' + result.reason)
  //     })
  //   })
  // }, 1000)
}

// Register Init function with the MTLG framework
// The function passed to addGameInit will be used to initialize the game.
// Use the initialization to register levels and menus.

MTLG.addGameInit(initGame)

/*
 * sets parameters and optionally does further instantiations
 *
 * pOptions contain:
 * language: game's language
 * width: width of playing field in browser's full screen mode
 * height: height of playing field in browser's full screen mode
 * countdown: timespan that must pass without anything happening, before game requests a restart
 * fps: preset FPS for TweenJS
 */
const setupVars = function (pOptions) {
}
