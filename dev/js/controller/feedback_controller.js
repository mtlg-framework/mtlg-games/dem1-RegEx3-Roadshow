
const types = {
  color: 1,
  aura: 2,
  shake: 4,
  sound: 8,
  colorArea: 16,
  turnAround: 32
}
const posttypes = {
  fly: 1,
  disappear: 2
}
const _presets = { // Later: Fetch from manifest/worker/...
  posColor: 'green',
  negColor: 'red',
  duration: 1000,
  delay: 0,
  intensity: 1
}

const init = function (presets) {
  let prop
  for (prop in presets) {
    _presets[prop] = presets[prop] || _presets[prop]
  }
  preset(presets)
}

const preset = function (presets) {

}

const getFeedback = function (action, player) {
  const isPos = action.value
  const suffix = (isPos ? 'pos' : 'neg')
  const color = (isPos ? 'green' : 'red')
  const fbOptionsArray = [{
    type: types.color + types.aura,
    posttype: 0,
    intensity: 1,
    delay: 0,
    duration: 1000,
    category: null,
    params: {
      soundfile: 'feedback/word_' + suffix,
      color,
      isPositive: isPos
    },
    tangibleFb: {
      tcolor: color,
      sound: false,
      soundDelay: 0,
      soundDuration: 0,
      vibrate: false,
      vibrateDelay: 0,
      vibrateDuration: 0,
      light: false,
      lightDelay: 0,
      lightDuration: 0,
      tangibleAlias: ''
    }
  }]

  if (action.tangible) {
    const nativeTangibleID = action.tangible.tangibleAlias
    fbOptionsArray[0].tangibleFb.tangibleAlias = nativeTangibleID
  } else {
    if (player === undefined) {
      fbOptionsArray[0].tangibleFb.tangibleAlias = MTLG.getSettings().default.tangibleIDs[action.area.aId]
    }
  }

  fbOptionsArray[0].tangibleFb.sound = true
  fbOptionsArray[0].tangibleFb.vibrate = true
  fbOptionsArray[0].tangibleFb.light = true

  if (!isPos) {
    fbOptionsArray[0].tangibleFb.soundDuration = 1000
    fbOptionsArray[0].tangibleFb.vibrateDuration = 1000
    fbOptionsArray[0].tangibleFb.lightDuration = 1000
  } else {
    fbOptionsArray[0].tangibleFb.soundDuration = 20
    fbOptionsArray[0].tangibleFb.vibrateDuration = 20
    fbOptionsArray[0].tangibleFb.lightDuration = 1000
  }

  // TODO handle status and post type
  if (!isPos) {
    fbOptionsArray[0].type += types.shake
    fbOptionsArray[0].posttype += posttypes.fly
  } else {
    fbOptionsArray[0].type += types.turnAround
    fbOptionsArray[0].posttype += posttypes.disappear
  }

  return fbOptionsArray
}

export {
  init,
  getFeedback,
  preset
}
