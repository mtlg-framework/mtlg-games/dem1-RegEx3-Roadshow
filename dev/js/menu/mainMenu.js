
function drawMainMenu () { // eslint-disable-line no-unused-vars
  MTLG.menu.start()

  const stage = MTLG.getStageContainer()

  // array of paramaters dependent on the input in the search bar

  const mKeysALL = [] // all available games
  const mKeysCS = [] // just computer science games
  const mKeysCASUAL = [] // just casual games

  // fill the arrays of parameters
  for (let i = 0; i < MTLG.getSettings().all.csgames.length; i++) {
    mKeysCS.push(MTLG.getSettings().all.csgames[i].key)
    mKeysALL.push(MTLG.getSettings().all.csgames[i].key)
  }

  for (let i = 0; i < MTLG.getSettings().all.casualgames.length; i++) {
    mKeysCASUAL.push(MTLG.getSettings().all.casualgames[i].key)
    mKeysALL.push(MTLG.getSettings().all.casualgames[i].key)
  }

  console.log(mKeysCS.toString())
  console.log(mKeysCASUAL.toString())
  console.log(mKeysALL.toString())

  const searchedMode = (window.location.search).toUpperCase()
  console.log(searchedMode)

  let _circleMenu = MTLG.utils.gameUtils.circleMenu()

  // fill the cirle menu in addiction to the searched mode
  switch (searchedMode) {
    case '?CS':
      _circleMenu = MTLG.utils.gameUtils.circleMenu({
        mKeys: mKeysCS,
        mText: '32px Arial',
        mTextColor: 'rgb(255, 255, 255)',
        mColor: ['rgba(119, 249, 5, 0.6)', 'rgba(249, 71, 5, 0.6)', 'rgba(20, 83, 246, 0.6)', 'rgba(194, 20, 246, 0.6)'],
        mBG: [{ src: 'img/background/eva.jpg' }, { src: 'img/background/RegEx.jpg' }],
        mPlBG: [{ src: 'img/background/eva.jpg', tx: 500, ty: 300, scale: 0.8 }, { src: 'img/background/RegEx.jpg', tx: 500, ty: 300, scale: 0.3 }],
        shadowColor: '#000000'
      })
      break

    case '?CASUAL':
      _circleMenu = MTLG.utils.gameUtils.circleMenu({
        mKeys: mKeysCASUAL,
        mText: '32px Arial',
        mTextColor: 'rgb(255, 255, 255)',
        mColor: ['rgba(119, 249, 5, 0.6)', 'rgba(249, 71, 5, 0.6)', 'rgba(20, 83, 246, 0.6)', 'rgba(194, 20, 246, 0.6)'],
        mBG: [{ src: 'img/background/animals.jpg' }, { src: 'img/background/Words.jpg', tx: 3000 * 0.8, ty: 0, scale: 0.8 }, { src: 'img/background/Chemie.jpg', tx: 0, ty: 30, scale: 0.7 }],
        mPlBG: [{ src: 'img/background/animals.jpg', tx: 100, ty: 100, scale: 0.1 }, { src: 'img/background/Words.jpg', tx: 100, ty: 100, scale: 0.3 }, { src: 'img/background/Chemie.jpg', tx: 100, ty: 100, scale: 0.3 }],
        shadowColor: '#000000'
      })
      break

    default:
      _circleMenu = MTLG.utils.gameUtils.circleMenu({
        mKeys: mKeysALL,
        mText: '32px Arial',
        mTextColor: 'rgb(255, 255, 255)',
        mColor: ['rgba(119, 249, 5, 0.6)', 'rgba(249, 71, 5, 0.6)', 'rgba(20, 83, 246, 0.6)', 'rgba(194, 20, 246, 0.6)'],
        mBG: [{ src: 'img/background/eva.jpg' }, { src: 'img/background/RegEx.jpg' }, { src: 'img/background/animals.jpg' }, { src: 'img/background/Words.jpg', tx: 3000 * 0.8, ty: 0, scale: 0.8 }, { src: 'img/background/Chemie.jpg', tx: 0, ty: 30, scale: 0.7 }],
        mPlBG: [{ src: 'img/background/eva.jpg', tx: 100, ty: 100, scale: 0.1 }, { src: 'img/background/RegEx.jpg', tx: 500, ty: 500, scale: 0.3 }, { src: 'img/background/animals.jpg', tx: 100, ty: 100, scale: 0.1 }, { src: 'img/background/Words.jpg', tx: 100, ty: 100, scale: 0.3 }, { src: 'img/background/Chemie.jpg', tx: 100, ty: 100, scale: 0.3 }],
        shadowColor: '#000000'
      })
      break
  }

  _circleMenu.addDragFeedback()
  stage.addChild(_circleMenu)

  MTLG.clearBackground()
  MTLG.setBackgroundImageFill('background/brick_bg.jpg')

  // stage.addChild(startContainer, startContainerClone)
}

export {
  drawMainMenu
}
