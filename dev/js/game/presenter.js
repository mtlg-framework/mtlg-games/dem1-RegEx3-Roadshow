const fbC = require('../controller/feedback_controller')
const xAPI = require('../controller/xapi.js')

let model
let view
let presets
let deactivated

// Variables
// var plusScore = 2; //Score that is added for correct regEx
// var minusScore = -1; //Score that is added for incorrect regEx

function init (_model, _view, prop) {
  if (model) {
    model.gameTimer.stop()
  }
  model = _model || {}
  view = _view || {}
  presets = prop || presets

  model.init(presets)
  view.init(model.getGameStatus())

  if (model.getGameStatus().endCondition === 'timer') {
    model.gameTimer.set(model.getGameStatus().maxTime, timerCallback)
    model.gameTimer.start()
  }
  deactivated = false

  // to save regExp to JSON
  // eslint-disable-next-line no-extend-native
  Object.defineProperty(RegExp.prototype, 'toJSON', {
    value: RegExp.prototype.toString
  })
}

function dragWord (target, state) {
  model.setWordDragged(target.wId, state)
}

function verifyWord (wordView, areaView, e) {
  if (deactivated) {
    return
  }; // No verification after game end, dragged cards can no longer trigger additional actions in transition screen.

  let player // default undefined

  // var gameMode = model.getGameStatus().gameMode;
  const status = model.checkWordStatus(wordView.wId, areaView.aId)

  const wId = wordView.wId
  const aId = areaView.aId

  if (status) {
    if (model.addCardToArea(wId, aId)) { // Try to add card to area, only increase score etc afterwards
      model.getCardById(wId).isMatched = true
      MTLG.loadSettings({
        default: {
          countAllRightMatches: MTLG.getSettings().default.countAllRightMatches + 1
        }
      })
      switch (model.getGameStatus().scoreType) {
        case 'different':
          model.increaseScoreOfArea(aId, MTLG.getSettings().default.plusScore)
          break
        case 'same':
          model.increaseAllScores(MTLG.getSettings().default.plusScore)
          break
        default:
      }

      if (MTLG.getSettings().default.endCondition === 'timer') {
        const level = 'level' + model.getGameStatus().level
        const pWordPool = model.getGameStatus().wordsArr[level][0]
        endlessHelper(wId, pWordPool)
      }
    }
  } else {
    MTLG.loadSettings({
      default: {
        countAllMissMatches: MTLG.getSettings().default.countAllMissMatches + 1
      }
    })
    switch (model.getGameStatus().scoreType) {
      case 'different':
        model.increaseScoreOfArea(aId, MTLG.getSettings().default.minusScore)
        break
      case 'same':
        model.increaseAllScores(MTLG.getSettings().default.minusScore)
        break
      default:
    }

    model.addWrongCardToArea(wId, aId)
  }

  if (status) { // Add card to new area and remove from old one TODO: is this always correct?
    model.removeCardFromAllAreas(wordView.wId)
    model.addCardToArea(wordView.wId, areaView.aId)
  }

  // Update View --------------
  view.update(model.getGameStatus(), model.getBestPlayers()) // TODO: Render additional symbol for best player

  // Feedback ------------------

  // About movement pattern
  // The coordinate progression is saved in wordView.moveCoord
  // The euclidian distance between coordinate points is saved in wordView.moveDiff

  // identify player if possible
  if (e.nativeEvent.tangible) {
    if (MTLG.getSettings().default.tangibleIDs.indexOf(e.nativeEvent.tangible.tangibleAlias) !== -1) {
      player = MTLG.getSettings().default.tangibleIDs.indexOf(e.nativeEvent.tangible.tangibleAlias)
    }
  }

  // save interaction
  // lcdmC.touchEvent({
  //   word: wordView,
  //   area: areaView,
  //   value: status,
  //   gameState: model.getGameStatus(),
  //   tangible: e.nativeEvent.tangible
  // }, player)

  // Feedback Controller
  const feedbackJSON = fbC.getFeedback({
    word: wordView,
    area: areaView,
    value: status,
    tangible: e.nativeEvent.tangible
  }, player)

  // show feedback
  view.feedback(areaView, wordView, status, feedbackJSON)
  // view.feedbackArea(, status);
  // view.feedbackWord(wordView, , status);

  // save interaction
  xAPI.wordEvent({
    word: wordView,
    area: areaView,
    value: status,
    gameState: model.getGameStatus(),
    tangible: e.nativeEvent.tangible
  }, player)

  if (model.getGameStatus().endCondition === 'level') {
    if (model.allCardsMatched()) {
      finishLevel()
    }
  }
}

function timerCallback () {
  finishLevel()
}

/**
   * Called once the level is done. Draws scores etc on transition screen in view.
   * View calls _levelUp after the review screen was dismissed
   */
function finishLevel () {
  deactivated = true // No more cards can be verified

  if (model.getGameStatus.endCondition === 'timer') {
    if (!model.gameTimer.timeUp()) {
      console.log('Time remaining in finishLevel. This should not happen.')
      console.log(model.gameTimer)
    }
    // Reset timer to prevent strange behavior where previous timer ends next level
    model.gameTimer.stop()
  }

  // Show Scores (currently in console)
  for (const player of model.getGameStatus().players) {
    console.log('Final scores: ' + player.score)
  }

  console.log(model.getGameStatus())

  // Chose correct Transition Screen
  if ((model.getGameStatus().level) === model.getGameStatus().maxLevel - 1) {
    view.levelTransition(MTLG.l('Game end'), _gameUp)
  } else {
    view.levelTransition(MTLG.l('Level end'), _levelUp)
  }
}

function _levelUp () {
  MTLG.lc.levelFinished({
    nextLevel: 3,
    level: 1
  })
}

function _gameUp () {
  MTLG.lc.levelFinished({
    nextLevel: 4
  })
}

/**
   * @function newGame
   * Called after the first level to transition to next level or end game if no other levels exist.
   */
function newGame (_view) {
  view = _view || {}

  // Always increase level, check if this was last level
  model.getGameStatus().level++
  if ((model.getGameStatus().endCondition === 'level' && model.getGameStatus().level > model.getGameStatus().maxLevel - 1) ||
      (model.getGameStatus().endCondition === 'timer' && model.gameTimer.timeUp() && model.getGameStatus().level > model.getGameStatus().maxLevel - 1)) {
    console.log('Game done!')

    model.gameTimer.stop()
    MTLG.lc.goToMenu() // you have to use the menu
  } else {
    if (model.getGameStatus().endCondition === 'timer') {
      // model.gameTimer.set(model.getGameStatus().maxTime, timerCallback)
      model.gameTimer.start()
    }

    model.preset(presets)

    view.init(model.getGameStatus()) // TODO: Reuse old view

    deactivated = false
  }
}

function getNewCards () {
  return model.getNewCards()
}

function endlessMultiHelper (wId) { // eslint-disable-line no-unused-vars
  const level = 'level' + model.getGameStatus().level
  const pWordPool = MTLG.getSettings().default.constantCoop2Words[level][0]
  console.log('endlesshelper')
  endlessHelper(wId, pWordPool)
}

/*
   * Creates a matching and two non-matching cards, deleting two non-matching cards in the process.
   * pWordPool is optional, default is constantWords
   */
function endlessHelper (wId, pWordPool) {
  // var currWord = model.getCardById(wId)
  const level = 'level' + model.getGameStatus().level
  const wordPool = pWordPool || model.getGameStatus().wordsArr[level][0]
  const bufferPool = wordPool.matching.filter((x) => {
    return !model.isMatchableExpression(x)
  })
  wordPool.matching = wordPool.matching.filter(model.isMatchableExpression) // TODO: This is a hack. This changes the original arrays
  wordPool.nonMatching = wordPool.nonMatching.concat(bufferPool)

  const pos = Math.floor(wordPool.matching.length * Math.random())
  const label = wordPool.matching[pos]
  model.addWord(label) // Add another matching card

  // Also delete two nonMatching cards to keep the number of cards equal
  // wordPool = MTLG.getSettings().default.constantWords[level]
  const deletedCards = []
  for (let i = 0; i < 2; i++) {
    do {
      const pos = Math.floor(model.getGameStatus().wordCards.length * Math.random())
      const currWord = model.getCardById(pos)
      if (deletedCards.indexOf(pos) > -1 || !currWord.isActive || wordPool.nonMatching.indexOf(currWord.word) < 0 || currWord.isDragged) {
        continue
      } else {
        deletedCards.push(pos)
        currWord.isActive = false // Deactivate card so that it can not be chosen twice
      }
    } while (deletedCards.length <= i)
  }
  for (let j = 0; j < 2; j++) { // And add two nonMatchin cards
    const level = 'level' + model.getGameStatus().level
    // console.log(MTLG.getSettings().default)
    const wordPool = MTLG.getSettings().default.cooperativeWords[level][0]
    const pos = Math.floor(wordPool.nonMatching.length * Math.random())
    const label = wordPool.nonMatching[pos]
    model.addWord(label)
  }
  view.newWords(model.getNewCards(), model.getGameStatus().wordCards.length - 3)
  view.deleteWords(deletedCards)
}

export {
  init,
  dragWord,
  newGame,
  verifyWord,
  getNewCards
}
