const SESSIONID = require('./utils/sessionInit.js')

export function Model (verifyFunction) { // eslint-disable-line no-unused-vars
  const _gStatus = {
    regEx: [],
    wordCards: [],
    newCards: [],
    players: [],
    areas: [],
    level: 0
    // maxTime: 0, // Duration of the game
  }

  class WordCard {
    constructor (label) {
      this.word = label
      // this.inL
      this.isActive = true // Is this card active (not removed, visible, ...)
      this.isMatched = false // Was the card matched to a regEx?
      this.matchedAreas = [] // Array containing the areas that have matched this word
      this.mismatchedAreas = [] // Array containing the areas that failed to match this word
      this.verify = function (pattern) {
        return (pattern && verifyFunction(this.word, pattern)) // regEx.test(this.word));
      }

      this.removeArea = function (aId) {
        const currArea = _gStatus.areas[aId]

        let index = this.matchedAreas.indexOf(currArea)
        if (index > -1) {
          this.matchedAreas.splice(index, 1)
        }
        index = this.mismatchedAreas.indexOf(currArea)
        if (index > -1) {
          this.mismatchedAreas.splice(index, 1)
        }
      }
    }
  }

  class RegEx {
    constructor (exp) {
      this.exp = exp
    }
  }

  class Player {
    constructor (user) {
      this.user = user // MTLG identity
      this.id = 0
      this.score = 0
      this.team = 0
      // this.feedbackOptions // TODO: Feedback options per player
      // this.traits // TODO: Information about player traits
      this.increaseScore = function (diff) {
        this.score += diff
      }
    }
  }

  class Area {
    constructor () {
      this.players = []
      this.regEx = []
      this.cardsFound = [] // Cards that were matched by this area
      this.wrongCards = [] // Cards that did not match this area
      this.maxCards = -1 // Number of cards that can be matched in this area
      this.isFull = false
      this.addPlayer = function (newPlayer) {
        this.players.push(newPlayer)
      }
      this.removePlayer = function (oldPlayer) {
        const index = this.players.indexOf(oldPlayer)
        if (index > -1) {
          this.players.splice(index, 1)
        }
      }
      this.addRegEx = function (newRegEx) {
        this.regEx.push(newRegEx)
      }
      this.removeRegEx = function (oldRegEx) {
        const index = this.regEx.indexOf(oldRegEx)
        if (index > -1) {
          this.regEx.splice(index, 1)
        }
      }
      this.hasPlayer = function (player) {
        return (this.players.indexOf(player) > -1)
      }
      this.hasRegEx = function (regEx) {
        return (this.regEx.indexOf(regEx) > -1)
      }

      this.addCard = function (newCard) {
        if (this.maxCards > -1 && this.cardsFound.length >= this.maxCards) {
          this.isFull = true
          return false
        }
        this.cardsFound.push(newCard)
        if (this.maxCards > -1 && this.cardsFound.length >= this.maxCards) {
          this.isFull = true
        }
        return true
      }
      this.addWrongCard = function (newCard) {
        this.wrongCards.push(newCard)
      }
      this.hasCard = function (card) {
        return (this.cardsFound.indexOf(card) > -1)
      }
      this.removeCard = function (card) {
        const index = this.cardsFound.indexOf(card)
        if (index > -1) {
          this.cardsFound.splice(index, 1)
        }
        if (this.maxCards > -1 && this.cardsFound.length < this.maxCards) {
          this.isFull = false
        }
      }
      this.setMaxCards = function (number) {
        this.maxCards = number
      }
    }
  }

  // Timer function
  // modified from https://stackoverflow.com/questions/3969475/javascript-pause-settimeout
  // function Timer(delay, callback) {
  class Timer {
    constructor () {
      let timerId, start, remaining, delay, callback

      this.set = function (_delay, _callback) {
        delay = _delay
        remaining = _delay
        callback = _callback
      }

      this.timeUp = function () {
        return (Date.now() - start >= remaining)
      }

      this.pause = function () {
        window.clearTimeout(timerId)
        remaining -= new Date() - start
      }

      this.resume = function () {
        start = new Date()
        window.clearTimeout(timerId)
        timerId = window.setTimeout(callback, remaining)
      }

      this.start = function () {
        start = new Date()
        timerId = window.setTimeout(callback, remaining)
      }

      this.stop = function () {
        window.clearTimeout(timerId)
        // start = 0;
        remaining = delay
      }
    }
  }

  this.init = function (presets) {
    let prop
    for (prop in presets) {
      if (presets[prop] === 0 || presets[prop] === false) {
        _gStatus[prop] = presets[prop]
      } else {
        _gStatus[prop] = presets[prop] || _gStatus[prop]
      }
    }

    SESSIONID.sessionInit()
    // if (sessionId) {
    //   // settingsVersion ++; // TODO find better way to handle this
    //   /* if (settingsVersion >= MTLG.getSettings().default.constantRegEx["level0"].length || settingsVersion >= MTLG.getSettings().default.constantWords["level0"].length) {
    //     settingsVersion = 0;
    //   } */
    // } else {
    //   console.log('No sessionID created!')
    //   // Generate session ID
    //   const date = Date.now()
    //   sessionId = hashCode('' + date) // Use old session ID or generate new one if none exists
    // }

    // Only create players in the beginning of the session
    // MTLG.getPlayerNumber()
    for (let i = 0, l = presets.playerNumber; i < l; i++) {
      _gStatus.players.push(new Player(MTLG.getPlayer(i)))
      // Generate playerId = $sessionId-$index
      _gStatus.players[_gStatus.players.length - 1].id = MTLG.getSettings().default.sessionId + '-' + i
    }

    this.preset(presets)
  }

  this.preset = function (presets) {
    _gStatus.gameMode = presets.gameMode
    _gStatus.useConstant = presets.useConstant

    // clear old level
    _gStatus.areas = []
    _gStatus.regEx = []
    _gStatus.wordCards = []

    // Max Time is the same for all modes
    _gStatus.maxTime = MTLG.getSettings().default.timerMaxTime

    // Presets that are specific for different modes
    switch (presets.gameMode) {
      case 'tutorial':
        _gStatus.endCondition = MTLG.getSettings().default.endCondition = 'level'
        _gStatus.scoreType = 'different'
        _gStatus.regExType = 'same'
        _gStatus.regExArr = MTLG.getSettings().default.tutorialRegEx
        _gStatus.wordsArr = MTLG.getSettings().default.tutorialWords
        break
      case 'collaborative':
        _gStatus.endCondition = MTLG.getSettings().default.endCondition = 'level'
        _gStatus.scoreType = 'same'
        _gStatus.regExType = 'same'
        _gStatus.regExArr = MTLG.getSettings().default.collaborativeRegEx
        _gStatus.wordsArr = MTLG.getSettings().default.collaborativeWords
        _gStatus.maxLevel = 2
        break
      case 'cooperative':
        _gStatus.endCondition = MTLG.getSettings().default.endCondition = 'timer'
        _gStatus.scoreType = 'same'
        _gStatus.regExType = 'different'
        _gStatus.regExArr = MTLG.getSettings().default.cooperativeRegEx
        _gStatus.wordsArr = MTLG.getSettings().default.cooperativeWords
        break
      case 'competitive':
        _gStatus.endCondition = MTLG.getSettings().default.endCondition = 'timer'
        _gStatus.scoreType = 'team'
        break
      default:
        console.log('No game mode selected.')
    }

    // constant or random mode
    if (_gStatus.useConstant) {
      this.presetConst()
    } else {
      this.presetRand()
    }
  }

  this.getGameStatus = function () {
    return _gStatus
  }

  this.checkWordStatus = function (wordId, areaId) {
    const area = _gStatus.areas[areaId]
    const word = _gStatus.wordCards[wordId]
    for (let i = 0; i < area.regEx.length; i++) {
      if (!word.verify(area.regEx[i].exp)) {
        return false
      }
    }
    return true
  }

  this.addWord = function (label) {
    _gStatus.wordCards.push(new WordCard(label))
    _gStatus.newCards.push(_gStatus.wordCards[_gStatus.wordCards.length - 1])
  }

  this.getNewCards = function () {
    const ret = _gStatus.newCards
    _gStatus.newCards = []
    return ret
  }

  this.addRegEx = function () {
    _gStatus.regEx.push(new RegEx())
  }

  this.removeWord = function (word) {
    const index = _gStatus.wordCards.indexOf(word)
    if (index > -1) {
      _gStatus.wordCards.splice(index, 1)
    }
  }

  this.removeRegEx = function (regEx) {
    const index = _gStatus.regEx.indexOf(regEx)
    if (index > -1) {
      _gStatus.regEx.splice(index, 1)
    }
  }

  this.getCardById = function (wId) {
    return _gStatus.wordCards[wId]
  }

  /**
   * word is the index, regEx is the real regEx object
   */
  this.checkWordInL = function (word, pattern) {
    return verifyFunction(word, pattern)
    // return regEx.exp.test(_gStatus.wordCards[word].word);
  }

  /**
   * Increases the scores of all players by diff
   */
  this.increaseAllScores = function (diff) {
    for (const player of _gStatus.players) {
      player.increaseScore(diff)
    }
  }

  this.increaseScoreOfPlayer = function (plId, diff) {
    _gStatus.players[plId].increaseScore(diff)
  }

  this.increaseScoreOfArea = function (arId, diff) {
    for (const player of _gStatus.areas[arId].players) {
      player.increaseScore(diff)
    }
  }

  this.increaseScoreOfTeam = function (arId, diff) {
    const area = _gStatus.areas[arId]
    const refPlayer = area.players[0]
    for (const player of _gStatus.players) {
      if (player.team === refPlayer.team) {
        player.increaseScore(diff)
      }
    }
  }

  /**
   * Returns an array including all players with the maximum score
   */
  this.getBestPlayers = function () {
    let maxScore = _gStatus.players[0].score
    let bestPlayers = []
    for (const player of _gStatus.players) {
      if (player.score > maxScore) {
        bestPlayers = [player]
        maxScore = player.score
      } else if (player.score === maxScore) {
        bestPlayers.push(player)
      }
    }
    return bestPlayers
  }

  this.addCardToArea = function (wId, aId) {
    return _gStatus.areas[aId].addCard(_gStatus.wordCards[wId])
  }
  this.addWrongCardToArea = function (wId, aId) {
    _gStatus.areas[aId].addWrongCard(_gStatus.wordCards[wId])
  }

  this.removeCardFromAllAreas = function (wId) {
    for (const area of _gStatus.areas) {
      if (area.hasCard(_gStatus.wordCards[wId])) {
        area.removeCard(_gStatus.wordCards[wId])
      }
    }
  }

  /**
   * Use this function to add an area to the matchedAreas of a word card.
   * This can be used to track which areas have already matched this card.
   * Note: This function sets isMatched of the word card if all areas were found.
   * Note2: This function UPDATES by deleting the area out of previous arrays
   */
  this.addAreaToWord = function (wId, aId) {
    // Check if matches, and add to corresponding array
    const matches = this.checkWordStatus(wId, aId)
    const currArea = _gStatus.areas[aId]
    // First remove the area, than add it again
    _gStatus.wordCards[wId].removeArea(aId)

    let matched
    if (matches) {
      matched = _gStatus.wordCards[wId].matchedAreas
      matched.indexOf(currArea) === -1 && matched.push(currArea)
    } else {
      matched = _gStatus.wordCards[wId].mismatchedAreas
      matched.indexOf(currArea) === -1 && matched.push(currArea)
    }
    if (this.matchedAllAreas(wId)) {
      _gStatus.wordCards[wId].isMatched = true
    }
  }

  this.gameTimer = new Timer()

  // ------------------------------------------------------------
  // ------------------- Game Modes -----------------------------
  // ------------------------------------------------------------
  // constant
  this.presetConst = function () {
    const _maxLevels = Object.keys(_gStatus.regExArr).length
    if (_gStatus.maxLevel > _maxLevels) _gStatus.maxLevel = _maxLevels

    switch (_gStatus.regExType) {
      case 'different':
        this.presetConstDifferentRegEx()
        break
      case 'same':
        this.presetConstSameRegEx()
        break
      default:
    }

    let t = 0
    for (let i = 0, l = _gStatus.players.length; i < l; i++) {
      _gStatus.areas.push(new Area())
      _gStatus.areas[i].addPlayer(_gStatus.players[i])
      if (_gStatus.regExType === 'different') t = i
      _gStatus.areas[i].addRegEx(_gStatus.regEx[t])
    }
  }

  this.presetConstSameRegEx = function () {
    do {
      _gStatus.regEx = []
      _gStatus.wordCards = []

      const level = 'level' + _gStatus.level
      _gStatus.regEx.push(new RegEx(_gStatus.regExArr[0][level]))

      for (let i = 0, l = _gStatus.nbrWords; i < l; i++) {
        const level = 'level' + _gStatus.level
        const wordTexts = _gStatus.wordsArr[level] // Load words from first level
        const wordPool = wordTexts[i % 2 ? 'matching' : 'nonMatching']
        let z
        if ((i % 2) && (wordPool.length === 2)) {
          z = Math.floor(2 * Math.random())
        } else {
          z = i % wordPool.length
        }
        _gStatus.wordCards.push(new WordCard(wordPool[z]))
      }
    } while (this.allCardsMatched() || this.allCardsValid())
  }

  this.presetConstDifferentRegEx = function () {
    do {
      _gStatus.regEx = []
      _gStatus.wordCards = []

      for (let i = 0, l = _gStatus.nbrWords; i < l; i++) {
        const wordTexts = _gStatus.wordsArr['level' + _gStatus.level] // Load words from first level
        const wordPool = wordTexts[0][i % 2 ? 'matching' : 'nonMatching']
        _gStatus.wordCards.push(new WordCard(wordPool[i % wordPool.length]))
      }

      for (let i = 0; i < _gStatus.players.length; i++) {
        const level = 'level' + _gStatus.level
        _gStatus.regEx.push(new RegEx(_gStatus.regExArr[level][0][i]))
      }
    } while (this.allCardsMatched()) // TODO: Some condition to split cards onto areas
  }

  // random
  this.presetRand = function () {
    // _gStatus.maxLevel = 2;
    do {
      _gStatus.regEx = []
      _gStatus.wordCards = []

      _gStatus.regEx.push(new RegEx())

      for (let i = 0, l = _gStatus.nbrWords; i < l; i++) {
        _gStatus.wordCards.push(new WordCard())
      }
    } while (this.allCardsMatched() || this.allCardsValid())

    for (let i = 0, l = _gStatus.players.length; i < l; i++) {
      _gStatus.areas.push(new Area())
      _gStatus.areas[i].addPlayer(_gStatus.players[i])
      _gStatus.areas[i].addRegEx(_gStatus.regEx[0])
    }
  }

  // ------------------------------------------------------------
  // ------------------- helper functions -------------------------
  // ------------------------------------------------------------

  this.isMatchable = function (card) {
    for (let i = 0; i < _gStatus.regEx.length; i++) {
      if (verifyFunction(card.word, _gStatus.regEx[i].exp)) {
      // if (_gStatus.regEx[i].exp.test(card.word)) {
        return true
      }
    }
    return false
  }

  this.isMatchableExpression = function (expression) {
    for (let i = 0; i < _gStatus.regEx.length; i++) {
      // if (_gStatus.regEx[i].exp.test(expression)) {
      if (verifyFunction(expression, _gStatus.regEx[i].exp)) {
        return true
      }
    }
    return false
  }

  this.matchesAllRegEx = function (card) {
    for (let i = 0; i < _gStatus.regEx.length; i++) {
      if (verifyFunction(card.word, _gStatus.regEx[i].exp)) {
      // if (!_gStatus.regEx[i].exp.test(card.word)) {
        return false
      }
    }
    return true
  }

  this.regExHaveIntersection = function () {
    for (const card of _gStatus.wordCards) {
      if (this.matchesAllRegEx(card)) {
        return true
      }
    }
    return false
  }

  this.allCardsMatched = function () {
    for (let i = 0; i < _gStatus.wordCards.length; i++) {
      if (!_gStatus.wordCards[i].isMatched && this.isMatchable(_gStatus.wordCards[i])) {
        return false
      }
    }
    return true
  }

  this.allCardsValid = function () {
    for (let i = 0; i < _gStatus.wordCards.length; i++) {
      if (!this.isMatchable(_gStatus.wordCards[i])) {
        return false
      }
    }
    return true
  }

  /**
   * Returns true if the given regEx matches not a single one of the existing word cards
   */
  this.noMatchesRegEx = function (regEx) {
    for (let i = 0; i < _gStatus.wordCards.length; i++) {
      if (_gStatus.wordCards[i].verify(regEx.exp)) {
        return false
      }
    }
    return true
  }

  /**
   * Returns true if the given regEx matches every word card existing
   */
  this.validRegEx = function (regEx) {
    for (let i = 0; i < _gStatus.wordCards.length; i++) {
      if (!_gStatus.wordCards[i].verify(regEx.exp)) {
        return false
      }
    }
    return true
  }

  /**
   * Function to check if the matchedAreas array of a word includes all the regEx
   * it could possibly match.
   */
  this.matchedAllAreas = function (wId) {
    const word = _gStatus.wordCards[wId]
    for (const currArea of _gStatus.areas) {
      if (this.checkWordStatus(wId, _gStatus.areas.indexOf(currArea)) && word.matchedAreas.indexOf(currArea) === -1) {
        return false
      }
    }
    return true
  }

  this.setWordDragged = function (wID, state) {
    _gStatus.wordCards[wID].isDragged = state
  }
}
