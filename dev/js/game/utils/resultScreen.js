const descriptionUtils = require('./descriptionUtils')

export const showResultScreen = function () {
  const shadow4allShapes = new createjs.Shadow('#000000', 5, 5, 10)
  const stage = MTLG.getStageContainer()
  MTLG.clearBackground()
  MTLG.setBackgroundImageFill(MTLG.getSettings().default.bgImage)
  console.log('Die Ergebnisseite wurde erfolgreich geladen. // The result screen loaded succesfully.')

  const container = new createjs.Container()
  container.x = container.y = 0

  descriptionUtils.headingBox(container, 105, 'Auswertung und Ergebnisse')

  descriptionUtils.boxes(container, 'img/icons/result_icon.png', 2)
  descriptionUtils.boxes(container, 'img/icons/check_icon.png', 4.5)
  descriptionUtils.boxes(container, 'img/icons/cross_icon.png', 7)

  const reachedPointsText = 'Es wurden ' + ((MTLG.getSettings().default.countAllMissMatches * MTLG.getSettings().default.minusScore) + (MTLG.getSettings().default.countAllRightMatches * MTLG.getSettings().default.plusScore)).toString() + ' Punkte erreicht!'
  descriptionUtils.textOutput(container, 300, ((MTLG.getOptions().width / 8) * 3) + 100, reachedPointsText, 'bold 40px "Arial', 'center')

  let fixGrammarForOneCards
  if (MTLG.getSettings().default.countAllRightMatches === 1) {
    fixGrammarForOneCards = ' Karte wurde '
  } else {
    fixGrammarForOneCards = ' Karten wurden '
  }

  const rightSortedCards = (MTLG.getSettings().default.countAllRightMatches).toString() + fixGrammarForOneCards + 'richtig zugeordnet!'
  descriptionUtils.textOutput(container, 550, ((MTLG.getOptions().width / 8) * 3) + 100, rightSortedCards, 'bold 40px "Arial', 'center')

  if (MTLG.getSettings().default.countAllMissMatches === 1) {
    fixGrammarForOneCards = ' Karte wurde '
  } else {
    fixGrammarForOneCards = ' Karten wurden '
  }

  const wrongSortedCards = (MTLG.getSettings().default.countAllMissMatches).toString() + fixGrammarForOneCards + 'falsch zugeordnet!'
  descriptionUtils.textOutput(container, 800, ((MTLG.getOptions().width / 8) * 3) + 100, wrongSortedCards, 'bold 40px "Arial', 'center')

  // Buttons
  let countButtons = 0

  // go to menu button
  const zumStartMenueTopRight = new createjs.Container()
  zumStartMenueTopRight.regX = zumStartMenueTopRight.regY = zumStartMenueTopRight.x = 0
  zumStartMenueTopRight.shadow = shadow4allShapes

  const zumStartMenueTopLeft = zumStartMenueTopRight.clone(true)
  const zumStartMenueBottomLeft = zumStartMenueTopRight.clone(true)
  const zumStartMenueBottomRight = zumStartMenueTopRight.clone(true)

  function getButtonProps (container) {
    return {
      text: MTLG.l('Zum Menü!'),
      width: (MTLG.getOptions().width / 8) * 1.5,
      color: '#21610B',
      textcolor: '#fff',
      textstyle: 'bold 40px "Arial',
      height: 70,
      place: {
        x: (MTLG.getOptions().width / 8) * 4 - ((MTLG.getOptions().width / 8) / 2),
        y: 100
      },
      draggable: true,
      cb: function () {
        countButtons += 1
        if (countButtons === 4) {
          MTLG.lc.goToMenu()
        }
        stage.removeChild(container)
      }
    }
  }

  MTLG.utils.gameUtils.Button(getButtonProps(zumStartMenueTopRight), zumStartMenueTopRight)
  MTLG.utils.gameUtils.Button(getButtonProps(zumStartMenueBottomRight), zumStartMenueBottomRight)
  MTLG.utils.gameUtils.Button(getButtonProps(zumStartMenueBottomLeft), zumStartMenueBottomLeft)
  MTLG.utils.gameUtils.Button(getButtonProps(zumStartMenueTopLeft), zumStartMenueTopLeft)

  descriptionUtils.ordersBoxes4Times(container, stage, zumStartMenueTopLeft, zumStartMenueTopRight, zumStartMenueBottomLeft, zumStartMenueBottomRight)
}
