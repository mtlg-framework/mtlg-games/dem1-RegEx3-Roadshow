const descriptionUtils = require('./descriptionUtils')

export const showHelpMenuBeforeGameStarts = function () {
  const shadow4allShapes = new createjs.Shadow('#000000', 5, 5, 10)
  const stage = MTLG.getStageContainer()
  MTLG.clearBackground()
  MTLG.setBackgroundImageFill(MTLG.getSettings().default.bgImage)
  console.log('Das Hilfe-Menü wurde erfolgreich geladen. // The help menu succesfully loaded.')

  const container = new createjs.Container()
  container.x = container.y = 0

  descriptionUtils.headingBox(container, 105, (MTLG.getSettings().default.gameName + ' - Regeln und Spielprinzip!'))

  descriptionUtils.boxes(container, 'img/icons/study_icon.png', 2)
  descriptionUtils.boxes(container, 'img/icons/exclamation_mark_icon.png', 4.5)
  descriptionUtils.boxes(container, 'img/icons/finishing_flag.png', 7)

  descriptionUtils.textOutput(container, 300, 220, MTLG.getSettings().description.de)

  const gamePrinciplesText = 'Das Spiel besteht aus zwei Leveln. Ziel ist es, die Abbildungen den richtigen Kategorien zuzuordnen. Es gibt einen gemeinsamen Punktestand für alle Spieler. Jede richtige Antwort liefert ' + (MTLG.getSettings().default.plusScore).toString() + ' Punkte. Jede falsche verändert den Punktestand um ' + (MTLG.getSettings().default.minusScore).toString() + ' Punkte.'
  descriptionUtils.textOutput(container, 550, 220, gamePrinciplesText)

  const gameEndText = 'Am Ende gibt es keine Verlierer, sondern nur Gewinner! - Denn ihr werdet mit Sicherheit alle etwas Neues dazu lernen! Beendet wird ein Level jeweils nach Ablauf einer Zeitdauer von ' + ((((MTLG.getSettings().default.timerMaxTime) / 1000) / 60).toFixed(0)).toString() + 'min und ' + ((((MTLG.getSettings().default.timerMaxTime) / 1000) % 60).toFixed(0)).toString() + 'sek.'
  descriptionUtils.textOutput(container, 800, 220, gameEndText)

  // Buttons
  let countButtons = 0

  const startButtonTopRightPlayers = new createjs.Container()
  startButtonTopRightPlayers.regX = startButtonTopRightPlayers.regY = startButtonTopRightPlayers.x = 0
  startButtonTopRightPlayers.shadow = shadow4allShapes

  const startButtonBottomRightPlayers = startButtonTopRightPlayers.clone(true)
  const startButtonBottomLeftPlayers = startButtonTopRightPlayers.clone(true)
  const startButtonTopLeftPlayers = startButtonTopRightPlayers.clone(true)

  function getButtonProps (container) {
    return {
      text: MTLG.l('Ich bin bereit!'),
      width: (MTLG.getOptions().width / 8) * 1.5,
      color: '#21610B',
      textcolor: '#fff',
      textstyle: 'bold 40px "Arial',
      height: 70,
      place: {
        x: (MTLG.getOptions().width / 8) * 4 - ((MTLG.getOptions().width / 8) / 2),
        y: 100
      },
      draggable: true,
      cb: function () {
        countButtons += 1
        if (countButtons === 4) {
          MTLG.lc.levelFinished({
            nextLevel: 3
          })
        }
        stage.removeChild(container)
      }
    }
  }

  MTLG.utils.gameUtils.Button(getButtonProps(startButtonTopRightPlayers), startButtonTopRightPlayers)
  MTLG.utils.gameUtils.Button(getButtonProps(startButtonBottomRightPlayers), startButtonBottomRightPlayers)
  MTLG.utils.gameUtils.Button(getButtonProps(startButtonBottomLeftPlayers), startButtonBottomLeftPlayers)
  MTLG.utils.gameUtils.Button(getButtonProps(startButtonTopLeftPlayers), startButtonTopLeftPlayers)

  console.log('Should the help menu be skipped? → ' + MTLG.getSettings().default.skipHelpMenu)

  if (MTLG.getSettings().default.skipHelpMenu === true) {
    MTLG.lc.levelFinished({
      nextLevel: 3
    })
  } else {
    // ordersBoxes4Times (stage, topLeft, topLeft, topRight, bottomLeft, bottomRight)
    descriptionUtils.ordersBoxes4Times(container, stage, startButtonTopLeftPlayers, startButtonTopRightPlayers, startButtonBottomLeftPlayers, startButtonBottomRightPlayers)
  }
}
