// Require Feedback Controller
const FBC = require('../../controller/feedback_controller.js')

// Require MVP
const MODEL = require('../model.js')
const VIEW = require('../view.js')
const PRESENTER = require('../presenter.js')

// Require games
const ANIMAL = require('../games/settingsAnimals.js')
const EVA = require('../games/settingsEVA.js')
const WORD = require('../games/settingsWord.js')
const REGEX = require('../games/settingsRegEx.js')
const CHEMIE = require('../games/settingsChemie.js')

/**
 * Startlevel that selects the right game to start
 * @return 1 if game starts for the first time, else 0
 */
const gameSelect = function () { // eslint-disable-line no-unused-vars
  console.log('Dummy Level to get parallel gamemodes and settings working with lc.')
  // MTLG.clearBackground()
  // MTLG.setBackgroundColor('black')

  // xAPI.init()
  // for (let i = 0; i < MTLG.getPlayerNumber(); i++) {
  //   xAPI.startSession(MTLG.getPlayerName(i))
  // }

  const game = MTLG.getSettings().default.game

  switch (game) {
    case 'RegEx':
      // load gamemode settings
      MTLG.loadSettings(REGEX.getRegExSettings())
      break
    case 'Word Game':
      MTLG.loadSettings(WORD.getWordSettings())
      break
    case 'EVA Game':
      MTLG.loadSettings(EVA.getEVASettings())
      break
    case 'Chemie Game':
      MTLG.loadSettings(CHEMIE.getChemieSettings())
      break
    case 'Animal und Plant Game':
      MTLG.loadSettings(ANIMAL.getAnimalPlantSettings())
      break
    default:
      MTLG.lc.goToMenu()
  }

  if (game) {
    MTLG.lc.levelFinished({
      nextLevel: 1
    })
  }
}

const gameSelectCheck = function (gameState) { // eslint-disable-line no-unused-vars
  if (!gameState) {
    return 1
  }
  if (!gameState.nextLevel) {
    return 1
  }
  return 0
}

/// //////////////////////////////////

const gameMenu = function (gameState) { // eslint-disable-line no-unused-vars
  // MTLG.getStage().removeAllEventListeners();
  // MTLG.getStage().removeAllChildren();
  // localStage = MTLG.getStage();
  // let localStage = MTLG.getStageContainer();

  // MTLG.getStage().addChild(localStage);
  drawGameMenu()
}

/**
 * Function that checks if Feld01 should be the next level
 * @param the current game state (an object containing only nextLevel in this case)
 * @return a number in [0,1] that represents that possibility.
 */
const checkLevel1 = function (gameState) { // eslint-disable-line no-unused-vars
  if (!gameState) {
    return 0 // assuming we should not start
  }
  if (gameState.nextLevel === 0) {
    return 0 // assuming we should not start
  }
  if (gameState.nextLevel === 1) {
    return 1
  } else {
    return 0
  }
}

const templateGame = function (gameState) { // eslint-disable-line no-unused-vars
  MTLG.getStageContainer().removeAllEventListeners()
  MTLG.getStageContainer().removeAllChildren()
  // localStage = MTLG.getStage();
  const localStage = MTLG.getStageContainer()

  // initialize feedback controller
  FBC.init()
  // console.log('Initialize LCDM controller')
  // lcdmC.init()

  // initialize MVP
  if (!gameState.level) {
    const _model = new MODEL.Model(MTLG.getSettings().default.verifyFunction)
    const _view = new VIEW.View(localStage)
    PRESENTER.init(_model, _view, MTLG.getSettings().default)
  } else {
    console.log('only view')
    const _view = new VIEW.View(localStage)
    PRESENTER.newGame(_view, MTLG.getSettings().default)
  }
}

/**
 * Function that checks if Feld01 should be the next level
 * @param the current game state (an object containing only nextLevel in this case)
 * @return a number in [0,1] that represents that possibility.
 */
function checkLevel2 (gameState) { // eslint-disable-line no-unused-vars
  if (!gameState) {
    return 0 // assuming we should not start
  }
  if (gameState.nextLevel === 1) {
    return 0 // assuming we should not start
  }
  if (gameState.nextLevel === 2) {
    return 1
  } else {
    return 0
  }
}

function checkLevel3 (gameState) { // eslint-disable-line no-unused-vars
  if (!gameState) {
    return 0 // assuming we should not start
  }
  if (gameState.nextLevel === 1 || gameState.nextLevel === 2) {
    return 0 // assuming we should not start
  }
  if (gameState.nextLevel === 3) {
    return 1
  } else {
    return 0
  }
}

function checkLevel4 (gameState) { // eslint-disable-line no-unused-vars
  if (!gameState) {
    return 0 // assuming we should not start
  }
  if (gameState.nextLevel === 1 || gameState.nextLevel === 2 || gameState.nextLevel === 3) {
    return 0 // assuming we should not start
  }
  if (gameState.nextLevel === 4) {
    return 1
  } else {
    return 0
  }
}

export {
  gameSelect,
  gameSelectCheck,
  checkLevel1,
  checkLevel2,
  checkLevel3,
  checkLevel4,
  templateGame
}
