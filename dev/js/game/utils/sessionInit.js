
export const sessionInit = function () { // eslint-disable-line no-unused-vars
  // MTLG.menu.start()

  // const stage = MTLG.getStageContainer()

  // create sessionId
  const date = Date.now()

  MTLG.loadSettings({
    default: {
      sessionId: hashCode('' + date)
    }
  })

  // sessionId
  // const startContainer = new createjs.Container()
  // startContainer.addChild(new MTLG.utils.gameUtils.Button({
  //   text: 'SessionID: ' + sessionId,
  //   place: {
  //     x: 0,
  //     y: 0
  //   },
  //   center: true,
  //   color: 'rgba(0, 0, 0, 0.85)',
  //   textcolor: 'rgb(255, 255, 255)',
  //   textstyle: 'bold 36px Arial',
  //   textSize: true
  // }))

  // startContainer.regX = startContainer.regY = 0
  // startContainer.x = MTLG.getOptions().width - 50
  // startContainer.y = MTLG.getOptions().height / 2
  // startContainer.rotation = 90

  // const startContainerClone = startContainer.clone(true)
  // startContainerClone.rotation = -90
  // startContainerClone.x = 50

  // stage.addChild(startContainer, startContainerClone)
}

/**
 * Taken from https://stackoverflow.com/questions/6122571/simple-non-secure-hash-function-for-javascript
 */
// eslint-disable-next-line no-unused-vars
const hashCode = function (inputStr) {
  let hash = 0
  if (inputStr.length === 0) {
    return hash
  }
  for (let i = 0; i < inputStr.length; i++) {
    const char = inputStr.charCodeAt(i)
    hash = ((hash << 5) - hash) + char
    hash = hash & hash // Convert to 32bit integer
  }
  if (hash < 1) {
    hash = hash * -1
  }
  return hash
}
