
export const drawGameMenu = function () {
  const stage = MTLG.getStageContainer()
  console.log('Das Game-Menü wurde erfolgreich geladen. // The game menu succesfully loaded.')

  // Title
  const template = MTLG.l(MTLG.getSettings().default.gameName)
  const variableTextSize = 50
  const title = new createjs.Text(template, 'bold ' + variableTextSize + 'px Arial', 'black')
  title.regX = title.getBounds().width / 2
  title.regY = title.getBounds().height / 2
  title.x = MTLG.getOptions().width / 2
  title.y = 150 // options.height / 4;
  title.textBaseline = 'alphabetic'

  // selectionContainer for game settings
  const selectionContainer = new createjs.Container()

  // Gamemodes
  MTLG.utils.gameUtils.buttonsSelectionGroup({
    cb: function () {
      MTLG.loadSettings({
        default: {
          gameMode: this.cbvar
        }
      })
      this.setColor('rgba(0, 155, 0, 0.7)')
      this.resetGroupColor()
      this.showNote()
    },
    cbvar: 'value',
    delta: 20,
    key: 'gameMode',
    default: MTLG.getSettings().default.gameMode
  }, MTLG.getSettings().all.modes, {
    x: 0,
    y: 0
  }, selectionContainer)

  // timer
  MTLG.utils.gameUtils.buttonNbrGroup({
    delta: 20,
    adjust: 1000, // adjust the number
    steps: 10
  }, {
    key: 'timerMaxTime',
    keyMax: 'timerMaxTimeMax',
    keyMin: 'timerMaxTimeMin'
  }, {
    x: 460,
    y: 0
  }, selectionContainer)

  // Player
  MTLG.utils.gameUtils.buttonNbrGroup({
    delta: 20
  }, {
    key: 'playerNumber',
    keyMax: 'playerMax',
    keyMin: 'playerMin'
  }, {
    x: 690,
    y: 0
  }, selectionContainer)

  // words
  MTLG.utils.gameUtils.buttonNbrGroup({
    delta: 20
  }, {
    key: 'nbrWords',
    keyMax: 'wordsMax',
    keyMin: 'wordsMin'
  }, {
    x: 920,
    y: 0
  }, selectionContainer)

  // delay
  MTLG.utils.gameUtils.buttonNbrGroup({
    delta: 20
  }, {
    key: 'delayWords',
    keyMax: 'delayWordsMax',
    keyMin: 'delayWordsMin'
  }, {
    x: 1150,
    y: 0
  }, selectionContainer)

  selectionContainer.regX = 0
  selectionContainer.regY = 0
  selectionContainer.x = MTLG.getOptions().width / 2 - 75 - 690
  selectionContainer.y = 200

  const startContainer = new createjs.Container()
  startContainer.regX = 0
  startContainer.regY = 0
  startContainer.x = MTLG.getOptions().width / 2 - 75
  startContainer.y = 700

  MTLG.utils.gameUtils.Button({
    text: MTLG.l('Main Menu'),
    place: {
      x: 0,
      y: 60
    },
    cb: MTLG.lc.goToMenu
  }, startContainer)

  MTLG.utils.gameUtils.Button({
    text: MTLG.l('start!'),
    place: {
      x: 0,
      y: 140
    },
    cb: function () {
      MTLG.lc.levelFinished({
        nextLevel: 2
      })
    }
  }, startContainer)

  // Background
  // MTLG.setBackgroundImageFill('background/brown-1866661.jpg') // pixabay

  console.log('Should the game menu be skipped? → ' + MTLG.getSettings().default.skipMenu)
  if (MTLG.getSettings().default.skipMenu === true) {
    MTLG.lc.levelFinished({
      nextLevel: 2
    })
  } else {
    stage.addChild(title)
    stage.addChild(selectionContainer)
    stage.addChild(startContainer)
  }
}
