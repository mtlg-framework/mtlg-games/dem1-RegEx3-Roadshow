
const checkIfMenuCanBeSkipped = function (backToMenu) {
  console.log(MTLG.getSettings().default)
  if (('topRight' in MTLG.getSettings().default) && ('topLeft' in MTLG.getSettings().default) && ('bottomRight' in MTLG.getSettings().default) && ('bottomLeft' in MTLG.getSettings().default)) {
    delete MTLG.getSettings().default.topRight
    delete MTLG.getSettings().default.topLeft
    delete MTLG.getSettings().default.bottomRight
    delete MTLG.getSettings().default.bottomLeft
    if (backToMenu === true) {
      MTLG.lc.levelFinished({
        nextLevel: 3
      })
    } else {
      MTLG.lc.goToMenu()
    }
  }
}

const headingBox = function (container, yPosition, gameName) {
  const shadow4allShapes = new createjs.Shadow('#000000', 5, 5, 10)
  const heading = new createjs.Shape()
  heading.shadow = shadow4allShapes
  heading.graphics.c().f('#21610B').dr((MTLG.getOptions().width / 8), 100 / 2, (MTLG.getOptions().width / 8) * 6, 100)

  const headingText = new createjs.Text()
  headingText.text = gameName
  headingText.color = '#FFF'
  headingText.font = 'bold 45px "Arial'
  headingText.set({
    textAlign: 'center',
    textBaseline: 'middle',
    x: MTLG.getOptions().width / 2,
    y: yPosition
  })
  container.addChild(heading)
  container.addChild(headingText)
}

const boxes = function (container, icon, scaling) {
  const shadow4allShapes = new createjs.Shadow('#000000', 5, 5, 10)
  const image = new createjs.Bitmap(icon)
  image.x = (MTLG.getOptions().width / 8) + 36
  image.y = (100 * scaling) + 36

  const iconBackground = new createjs.Shape()
  iconBackground.shadow = shadow4allShapes
  iconBackground.graphics.c().f('#FF6600').dr((MTLG.getOptions().width / 8), 100 * scaling, 200, 200)

  const textBackground = new createjs.Shape()
  textBackground.shadow = shadow4allShapes
  textBackground.graphics.c().f('#FFCC75').dr((MTLG.getOptions().width / 8) + 200, (100 * scaling) + 10, ((MTLG.getOptions().width / 8) * 6) - 200, 180)

  container.addChild(textBackground)
  container.addChild(iconBackground)
  container.addChild(image)
}

const textOutput = function (container, yPosition, xOrientation, inputText, textsize, textalign) {
  const text = new createjs.Text()
  text.text = inputText
  text.color = '#000'
  text.font = textsize || 'bold 25px "Arial'
  text.lineWidth = ((MTLG.getOptions().width / 8) * 6) - 240
  text.lineHeight = 35
  text.set({
    textAlign: textalign || 'left',
    textBaseline: 'top',
    x: (MTLG.getOptions().width / 8) + xOrientation,
    y: yPosition - ((text.getMeasuredHeight() / 2) * 0.85)
  })
  container.addChild(text)
}

const ordersBoxes4Times = function (container, stage, topLeft, topRight, bottomLeft, bottomRight) {
  let containerTopLeftCorner = new createjs.Container()
  let containerTopRightCorner = new createjs.Container()
  let containerBottomRightCorner = new createjs.Container()
  let containerBottomightCorner = new createjs.Container()

  containerTopLeftCorner = container.clone(true)
  containerTopRightCorner = container.clone(true)
  containerBottomRightCorner = container.clone(true)
  containerBottomightCorner = container.clone(true)

  // setTransform ([x=0], [y=0], [scaleX=1], [scaleY=1], [rotation=0], [skewX=0], [skewY=0], [regX=0], [regY=0])
  containerTopLeftCorner.setTransform(90, 25, 0.4, 0.4, 180, 0, 0, MTLG.getOptions().width, MTLG.getOptions().height)
  containerTopRightCorner.setTransform((MTLG.getOptions().width / 2) + 90, 25, 0.4, 0.4, 180, 0, 0, MTLG.getOptions().width, MTLG.getOptions().height)
  containerBottomRightCorner.setTransform(MTLG.getOptions().width - 100, MTLG.getOptions().height - 25, 0.4, 0.4, 0, 0, 0, MTLG.getOptions().width, MTLG.getOptions().height)
  containerBottomightCorner.setTransform((MTLG.getOptions().width / 2) - 100, MTLG.getOptions().height - 25, 0.4, 0.4, 0, 0, 0, MTLG.getOptions().width, MTLG.getOptions().height)

  stage.addChild(containerTopLeftCorner, containerTopRightCorner, containerBottomRightCorner, containerBottomightCorner)

  // setTransform ([x=0], [y=0], [scaleX=1], [scaleY=1], [rotation=0], [skewX=0], [skewY=0], [regX=0], [regY=0])
  topLeft.setTransform(((MTLG.getOptions().width * 3) / 4) + 50, (MTLG.getOptions().height / 8) * 1.4, 1, 1, 180)
  topRight.setTransform(((MTLG.getOptions().width * 5) / 4) + 50, (MTLG.getOptions().height / 8) * 1.4, 1, 1, 180)
  bottomLeft.setTransform(-(MTLG.getOptions().width / 4) - 50, (MTLG.getOptions().height / 8) * 6.6, 1, 1, 0)
  bottomRight.setTransform((MTLG.getOptions().width / 4) - 50, (MTLG.getOptions().height / 8) * 6.6, 1, 1, 0)

  stage.addChild(topLeft, topRight, bottomLeft, bottomRight)
}

export {
  boxes,
  textOutput,
  headingBox,
  ordersBoxes4Times,
  checkIfMenuCanBeSkipped
}
