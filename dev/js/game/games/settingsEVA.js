
export function getEVASettings () { // eslint-disable-line no-unused-vars
  /* For the elementary school students personally,
  I prefer that there are only two categories per level.
  In the game there are therefore two categories per level
  on one side of the table. */

  const pattern = {

    level0: [
      ['Eingabe', 'Verarbeitung', 'Verarbeitung', 'Eingabe']
    ],

    level1: [
      ['Speicherung', 'Ausgabe', 'Ausgabe', 'Speicherung']
    ]

  }

  const words = {

    level0: [{
      matching: ['controller', 'mouse2', 'mouse', 'keyboard', 'keyboard2', 'joystick', 'remote', 'scanner', 'mic', 'webcam', 'webcam2', 'floppy_disc', 'flippy_disc', 'sd_card', 'sd_card2', 'usb2', 'harddrivedisc', 'cd', 'usb', 'external_storage'],
      nonMatching: ['sound_system', 'sound_system2', 'headset', 'headphones', 'printer', 'printer2', 'beamer', 'monitor', 'monitor2', 'motherboard', 'motherboard2', 'graphiccard', 'graphiccard2', 'microcontroller', 'cpu', 'cpu2', 'ram']
    }],

    level1: [{
      matching: ['sound_system', 'sound_system2', 'headset', 'headphones', 'printer', 'printer2', 'beamer', 'monitor', 'monitor2', 'motherboard', 'motherboard2', 'graphiccard', 'graphiccard2', 'microcontroller', 'cpu', 'cpu2', 'ram'],
      nonMatching: ['controller', 'mouse2', 'mouse', 'keyboard', 'keyboard2', 'joystick', 'remote', 'scanner', 'mic', 'webcam', 'webcam2', 'floppy_disc', 'flippy_disc', 'sd_card', 'sd_card2', 'usb2', 'harddrivedisc', 'cd', 'usb', 'external_storage']
    }]

  }

  const verifyFunction = function (word, pattern) {
    const _solution = [{
      pattern: 'Eingabe',
      words: ['controller', 'mouse2', 'mouse', 'keyboard', 'keyboard2', 'joystick', 'remote', 'scanner', 'mic', 'webcam', 'webcam2']
    }, {
      pattern: 'Ausgabe',
      words: ['sound_system', 'sound_system2', 'headset', 'headphones', 'printer', 'printer2', 'beamer', 'monitor', 'monitor2']
    }, {
      pattern: 'Verarbeitung',
      words: ['motherboard', 'motherboard2', 'graphiccard', 'graphiccard2', 'microcontroller', 'cpu', 'cpu2', 'ram']
    }, {
      pattern: 'Speicherung',
      words: ['floppy_disc', 'flippy_disc', 'sd_card', 'sd_card2', 'usb2', 'harddrivedisc', 'cd', 'usb', 'external_storage']
    }]

    function search (el) {
      if (el.pattern === pattern) {
        return el.words.includes(word)
      }
      return false
    } if (_solution.find(search) !== undefined) {
      return true
    }
    return false
  }

  const regExToString = function (regEx) {
    return regEx
  }

  const settings = {

    default: {
      gameName: 'EVA(S)-Spiel',
      bgImage: 'background/eva.jpg',
      bgPath: 'evapics/',
      regExToString,
      verifyFunction,
      gameMode: 'cooperative',
      useConstant: true, // use constant regEx/word or generate randomly
      nbrWords: 15,
      timerMaxTime: 1 * 100 * 1000, // per Game max time in milli-seconds
      delayWords: 0, // delay till the words will appear
      plusScore: 1,
      minusScore: -1,
      alphabet: ['eva'],
      cooperativeRegEx: pattern,
      cooperativeWords: words,
      wordView: 'pictures'
    },

    design: {
      pattern: {
        font: 'Arial',
        fontType: '',
        fontColor: 'white',
        fontBackground: 'rgba(38, 38, 38, 0.7)',
        fontAlign: 'center',
        background: 'rgba(38, 38, 38, 0.2)'
      }
    },
    description: {
      de: 'EVA(S) steht für Eingabe, Verarbeitung, Ausgabe und Speicherung. Dabei handelt es sich um die grundlegende Funktionsweise eines Informatiksystems. Es werden Eingaben getätigt, diese werden verarbeitet und anschließend eine Ausgabe erzeugt.',
      eng: 'EVA(S) stands for input, processing, output and storage. This is the basic functionality of an IT system. Inputs are made, processed and output generated.'
    },
    all: {
      modes: [{
        key: 'cooperative',
        note: 'some devices for the EVA-principle, two levels, sorted by "Input & Storage" and "Output & Processing"'
      }]
    }
  }

  return settings
};
