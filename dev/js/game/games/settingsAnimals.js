export function getAnimalPlantSettings () {
  const pattern = {
    level0: [
      ['Wiese & Hecke', 'See & Teich', 'See & Teich', 'Wiese & Hecke']
    ],
    level1: [
      ['Wald', 'Bergwelt', 'Bergwelt', 'Wald']
    ],
    level2: [
      ['Wattenmeer', 'Arktis', 'Arktis', 'Wattenmeer']
    ]
  }

  const words = {
    level0: [{
      matching: ['wildeMoehre', 'schwebfliege', 'wespe', 'schafgarbe', 'heuschrecke', 'maulwurf', 'spitzwegerich', 'rotklee', 'klee', 'lowenzahn_ganz', 'loewenzahn', 'regenwurm', 'neuntoeter', 'weinbergschnecke', 'rotkehlchen', 'schmetterling', 'graureiher', 'graugans', 'seerose', 'fisch2', 'wasserlaeufer', 'wasserpest', 'teichrohrsaenger', 'fischotter', 'nutria', 'teichmolch', 'stockente', 'schilf', 'rohrkolben', 'fisch1', 'weide', 'pfeilkraut', 'libelle'],
      nonMatching: ['tanne', 'kieferzapfen', 'fliegenpilz', 'brombeere', 'wildschwein', 'farn', 'waldameise', 'waldmaus', 'eichhoernchen', 'uhu', 'specht', 'reh', 'baerlauch', 'schluesselblume', 'maigloeckchen']
    }],
    level1: [{
      matching: ['tanne', 'kieferzapfen', 'fliegenpilz', 'brombeere', 'wildschwein', 'farn', 'waldameise', 'waldmaus', 'eichhoernchen', 'uhu', 'specht', 'reh', 'baerlauch', 'schluesselblume', 'maigloeckchen', 'murmeltier', 'steinadler', 'schneehase', 'bartgeier', 'alpendohle', 'hermelin', 'braunbaer', 'enzian', 'edelweiss'],
      nonMatching: ['wattwurm', 'strandkrabbe', 'herzmuschel', 'nordseegarnele', 'austernfischer', 'silbermoewe', 'brandgans']
    }],
    level2: [{
      matching: ['wattwurm', 'strandkrabbe', 'herzmuschel', 'nordseegarnele', 'austernfischer', 'silbermoewe', 'brandgans', 'robbe', 'pinguin', 'eisbaer'],
      nonMatching: ['wildeMoehre', 'schwebfliege', 'wespe', 'schafgarbe', 'heuschrecke', 'maulwurf', 'spitzwegerich', 'rotklee', 'klee', 'lowenzahn_ganz', 'loewenzahn', 'regenwurm', 'neuntoeter', 'weinbergschnecke', 'rotkehlchen', 'schmetterling']
    }]
  }

  const verifyFunction = function (word, pattern) {
    const _solution = [{
      pattern: 'Wiese & Hecke',
      words: ['wildeMoehre', 'schwebfliege', 'wespe', 'schafgarbe', 'heuschrecke', 'maulwurf', 'spitzwegerich', 'rotklee', 'klee', 'lowenzahn_ganz', 'loewenzahn', 'regenwurm', 'neuntoeter', 'weinbergschnecke', 'rotkehlchen', 'schmetterling']
    }, {
      pattern: 'See & Teich',
      words: ['graureiher', 'graugans', 'seerose', 'fisch2', 'wasserlaeufer', 'wasserpest', 'teichrohrsaenger', 'fischotter', 'nutria', 'teichmolch', 'stockente', 'schilf', 'rohrkolben', 'fisch1', 'weide', 'pfeilkraut', 'libelle']
    }, {
      pattern: 'Wald',
      words: ['tanne', 'kieferzapfen', 'fliegenpilz', 'brombeere', 'wildschwein', 'farn', 'waldameise', 'waldmaus', 'eichhoernchen', 'uhu', 'specht', 'reh', 'baerlauch', 'schluesselblume', 'maigloeckchen']
    }, {
      pattern: 'Bergwelt',
      words: ['murmeltier', 'steinadler', 'schneehase', 'bartgeier', 'alpendohle', 'hermelin', 'braunbaer', 'enzian', 'edelweiss']
    }, {
      pattern: 'Wattenmeer',
      words: ['wattwurm', 'strandkrabbe', 'herzmuschel', 'nordseegarnele', 'austernfischer', 'silbermoewe', 'brandgans']
    }, {
      pattern: 'Arktis',
      words: ['robbe', 'pinguin', 'eisbaer']
    }]

    function search (el) {
      if (el.pattern === pattern) {
        return el.words.includes(word)
      }
      return false
    }

    if (_solution.find(search) !== undefined) {
      return true
    }

    return false
  }

  const regExToString = function (regEx) {
    return regEx
  }

  const settings = {
    default: {
      gameName: 'Tiere und Pflanzen',
      bgImage: 'background/animals.jpg',
      bgPath: 'animals_and_plants_pics/',
      regExToString,
      verifyFunction,
      gameMode: 'cooperative',
      useConstant: true, // Use constant regEx/word or generate randomly
      nbrWords: 25,
      timerMaxTime: 1 * 120 * 1000, // Per Game max time in milli-seconds
      delayWords: 0, // delay till the words will appear
      plusScore: 2,
      minusScore: -1,
      alphabet: ['animals'],
      cooperativeRegEx: pattern,
      cooperativeWords: words,
      wordView: 'pictures'
    },
    description: {
      de: 'Mit diesem Lernspiel lernst du Tiere und Pflanzen aus den Lebensräumen Wiese und Hecke, See und Teich, Wald, Bergwelt, Wattenmeer und Arktis kennen. ',
      eng: 'With this educational game you will get to know animals and plants from the habitats meadow and hedge, lake and pond, forest, mountain world and Wadden Sea.'
    },
    design: {
      pattern: {
        font: 'Arial',
        fontType: '',
        fontColor: 'white',
        fontBackground: 'rgba(38, 38, 38, 0.7)',
        fontAlign: 'center',
        background: 'rgba(38, 38, 38, 0.2)'
      }
    },
    all: {
      modes: [{
        key: 'cooperative',
        note: 'Different regEx, one score, timer, four level.'
      }]
    }
  }

  return settings
}

/**
function getAnimalSettings () { // eslint-disable-line no-unused-vars
  let pattern = {
    level0: [
      ['Tiere mit A', 'Tiere mit S', 'Tiere mit K', 'Tiere mit E']
    ]
  }

  let words = {
    level0: [{
      matching: ['Ente', 'Esel','Elefant', 'Ameise','Affe','Adler', 'Schnecke', 'Schildkroete', 'Schlange', 'Krebs','Katze','Kamel'],
      nonMatching: ['Hai', 'Biene', 'Maus', 'Giraffe', 'Fisch']
    }],
    level1: [{
      matching: ['Auto', 'Bahn', 'Boot', 'Baum', 'Marmelade'],
      nonMatching: ['Ente', 'Koala', 'Ameise', 'Hai', 'Biene', 'Adler', 'Maus']
    }]
  }

  let verifyFunction = function (word, pattern) {
    let _solution = [{
      pattern: 'Tiere mit A',
      words: ['Ameise','Affe','Adler']
    }, {
      pattern: 'Tiere mit E',
      words: ['Ente', 'Esel','Elefant']
    }, {
      pattern: 'Tiere mit K',
      words: ['Krebs','Katze','Kamel']
    }, {
      pattern: 'Tiere mit S',
      words: ['Schnecke', 'Schildkroete', 'Schlange']
    }, {
      pattern: 'Pflanze',
      words: ['Baum']
    }, {
      pattern: 'Zug',
      words: ['Bahn']
    }, {
      pattern: 'Boot',
      words: ['Boot']
    }, {
      pattern: 'Essen',
      words: ['Marmelade']
    }]

    function search (el) {
      if (el.pattern === pattern) {
        return el.words.includes(word)
      }
      return false
    }

    if (_solution.find(search) !== undefined) {
      return true
    }

    return false
  }

  let regExToString = function (regEx) {
    return regEx
  }

  let settings = {
    default: {
      gameName: 'Tierzuordnung',
      bgImage: 'background/Animals.jpg',
      bgPath: 'animalpics/',
      regExToString: regExToString,
      verifyFunction: verifyFunction,
      gameMode: 'cooperative',
      useConstant: true, // Use constant regEx/word or generate randomly
      nbrWords: 25,
      timerMaxTime: 1 * 90 * 1000, // Per Game max time in milli-seconds
      delayWords: 0, // delay till the words will appear
      plusScore: 2,
      minusScore: -1,
      alphabet: ['animals'],
      cooperativeRegEx: pattern,
      cooperativeWords: words,
      wordView: 'pictures'
    },
    description: {
      de: 'Tiere lassen sich jeweils einer Art zuordnen, in diesem Spiel stehen zum Beispiel Vögel, Insekten, Fische und Landtiere zur Auswahl. Aber Achtung, es wird auch Objekte geben, die du nicht zuordnen kannst!',
      eng: 'Animals can each be assigned to a species, in this game, for example, birds, insects, fish and land animals are available. But be careful, there will also be objects that you cannot assign!'
    },
    design: {
      pattern: {
        font: 'Arial',
        fontType: '',
        fontColor: 'white',
        fontBackground: 'rgba(38, 38, 38, 0.7)',
        fontAlign: 'center',
        background: 'rgba(38, 38, 38, 0.2)'
      }
    },
    all: {
      modes: [{
        key: 'cooperative',
        note: 'Different regEx, one score, timer, four level.'
      }]
    }
  }

  return settings
}
*/
