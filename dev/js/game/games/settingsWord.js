export function getWordSettings () {
  const pattern = {
    level0: [
      ['Verben', 'Adjektive', 'Nomen', 'Konjunktionen']
    ]
  }

  const words = {
    level0: [{
      matching: ['laufen', 'reden', 'fahren', 'singen', 'tanzen', 'schnell', 'laut', 'klein', 'gelb', 'lustig', 'süß', 'Hai', 'Haus', 'Obst', 'Wiese', 'Stein', 'und', 'oder', 'als', 'wie', 'ob', 'dass'],
      nonMatching: ['hinter', 'auf', 'bei', 'für', 'wegen', 'zu', 'an', 'über', 'unter', 'ach', 'nanu', 'oh', 'pfui']
    }]
  }

  const verifyFunction = function (word, pattern) {
    const _solution = [{
      pattern: 'Verben',
      words: ['laufen', 'reden', 'fahren', 'singen', 'tanzen']
    }, {
      pattern: 'Adjektive',
      words: ['schnell', 'laut', 'klein', 'gelb', 'lustig', 'süß']
    }, {
      pattern: 'Nomen',
      words: ['Hai', 'Haus', 'Obst', 'Wiese', 'Stein']
    }, {
      pattern: 'Konjunktionen',
      words: ['und', 'oder', 'als', 'wie', 'ob', 'dass']
    }]

    function search (el) {
      if (el.pattern === pattern) {
        return el.words.includes(word)
      }
      return false
    }

    if (_solution.find(search) !== undefined) {
      return true
    }

    return false
  }

  const regExToString = function (regEx) {
    return regEx
  }

  const settings = {
    default: {
      gameName: 'Wortarten',
      bgImage: 'background/Words.jpg',
      bgPath: '',
      regExToString,
      verifyFunction,
      gameMode: 'cooperative',
      useConstant: true, // Use constant regEx/word or generate randomly
      nbrWords: 25,
      timerMaxTime: 1 * 90 * 1000, // Per Game max time in milli-seconds
      delayWords: 0, // delay till the words will appear
      plusScore: 2,
      minusScore: -1,
      alphabet: ['words'],
      cooperativeRegEx: pattern,
      cooperativeWords: words,
      wordView: 'words'
    },
    description: {
      de: 'Wortarten sind eine Möglichkeit, die einzelnen Wörter einer Sprache zu kategorisieren. In diesem Spiel unterscheiden wir Verben, Adjektive, Nomen und Konjunktionen. Dies ist eine Auswahl der insgesamt 10 Wortarten der deutschen Sprache.',
      eng: 'Parts of speech are a way of categorizing the individual words in a language. In this game we differentiate between verbs, adjectives, nouns and conjunctions. This is a selection of a total of 10 parts of speech in the German language.'
    },
    design: {
      words: {
        font: 'Comic Sans MS',
        fontType: 'italic',
        fontColor: 'blue',
        background: 'white'
      },
      pattern: {
        font: 'Arial',
        fontType: 'italic',
        fontColor: 'rgb(243,242,242)',
        fontBackground: 'rgba(0, 0, 0, 0.8)',
        fontAlign: 'center',
        background: 'rgba(0, 0, 0, 0.3)'
      }
    },
    all: {
      modes: [{
        key: 'cooperative',
        note: 'Different regEx, one score, timer, four level.'
      }]
    }
  }

  return settings
}
